package com.booking.jsalib.facebookwallpaper.ui.fragments

import android.annotation.SuppressLint
import android.app.WallpaperManager
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioGroup
import com.booking.jsalib.facebookwallpaper.R
import com.booking.jsalib.facebookwallpaper.ui.Activity.MainActivity
import com.squareup.picasso.Picasso
import android.util.DisplayMetrics
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.face.FaceDetector
import java.io.ByteArrayOutputStream
import android.graphics.*
import android.os.AsyncTask
import android.widget.LinearLayout
import com.booking.jsalib.facebookwallpaper.model.SignificantOther
import com.google.firebase.crash.FirebaseCrash


@SuppressLint("ValidFragment")
class StepFourFragment : StepFragment() {

    private lateinit var myWallpaperManager: WallpaperManager
    private lateinit var backgroundImageView: ImageView
    private lateinit var previewImageView: ImageView
    private lateinit var radioGroup: RadioGroup
    private lateinit var previewLinearLayout: LinearLayout
    private lateinit var imageProcessingLinearLayout: LinearLayout


    private var original: Bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
    private var face: Bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
    private var center: Bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)

    private val target: com.squareup.picasso.Target = object : com.squareup.picasso.Target {
        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
        }

        override fun onBitmapFailed(errorDrawable: Drawable?) {
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            original = bitmap ?: original
            ImageTask().execute()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.step4, container, false)

        if (view != null) {
            backgroundImageView = mainActivity.findViewById(R.id.background_imageView) // this is the main activity
            myWallpaperManager = WallpaperManager.getInstance(activity)
            previewImageView = view.findViewById(R.id.preview_imageView)
            radioGroup = view.findViewById(R.id.radioGroup)
            previewLinearLayout = view.findViewById(R.id.preview_linearLayout)
            imageProcessingLinearLayout = view.findViewById(R.id.image_processing_linearLayout)

            radioGroup.setOnCheckedChangeListener({ _, i ->
                when (i) {
                    R.id.center_radioButton -> previewImageView.setImageBitmap(face)
                    R.id.face_radioButton -> previewImageView.setImageBitmap(center)
                }
                showNextButton(true)
            })
        }

        return view
    }


    override fun onShown() {
        try {
            showNextButton(false)
            showBackButton(true)
            val significantOther = getBundle()?.getSerializable(MainActivity.SO_KEY) as SignificantOther

            Picasso.with(activity)
                    .load(significantOther.pictureData.picture.url)
                    .resize(backgroundImageView.width, backgroundImageView.height)
                    .centerCrop()
                    .into(backgroundImageView)

            Picasso.with(activity)
                    .load(significantOther.pictureData.picture.url)
                    .into(target)

            showProgress(true)
        } catch (e: Exception) {
            FirebaseCrash.log("Loading the significant other")
            FirebaseCrash.report(e)
            activity?.finish() //check if we ever need that
        }
    }

    private fun faceCrop(): Bitmap {
        val desiredWidth = myWallpaperManager.desiredMinimumWidth
        val desiredHeight = myWallpaperManager.desiredMinimumHeight
        val detector = FaceDetector.Builder(activity)
                .setTrackingEnabled(false)
                .build()
        val scale = desiredHeight.toFloat() / original.height

        val frame = Frame.Builder().setBitmap(original).build()

        val faces = detector.detect(frame)
        if (faces.size() == 0) {
            // couldn't detect any faceCrop
            return centerCrop()
        }
        var facesAverageCenterX = 0f
        var startXOfLeftMostFace = desiredWidth.toFloat()
        for (i in 0 until faces.size()) {
            val face = faces.valueAt(i)
            facesAverageCenterX += (face.position.x + face.width / 2)
            if (startXOfLeftMostFace > face.position.x)
                startXOfLeftMostFace = face.position.x
            if (face.position.x < 0)
                startXOfLeftMostFace = 0f
        }
        facesAverageCenterX /= faces.size()
        facesAverageCenterX *= scale
        startXOfLeftMostFace *= scale

        val scaledWidth = (scale * original.width).toInt()
        val metrics = DisplayMetrics()
        mainActivity.windowManager.defaultDisplay.getMetrics(metrics)
        val deviceWidth = metrics.widthPixels
        val imageCenterWidth = facesAverageCenterX
        // in case of small faces we don't want to start cutting from the start of the faceCrop
        // we want to start cutting relative to the centerCrop
        // if the faceCrop is very large cut from start of faceCrop
        var widthToCut = (imageCenterWidth - deviceWidth / 2)
        if (startXOfLeftMostFace < widthToCut) {
            widthToCut = startXOfLeftMostFace
        }
        val remainderWidth = scaledWidth - widthToCut // what is left of the width after cutting
        if (remainderWidth < deviceWidth) {
            widthToCut = (scaledWidth - deviceWidth).toFloat()
        }

        // in case of some calculation errors
        if (widthToCut < 0) {
            widthToCut = 0f
        }

        var scaledWallpaper = Bitmap.createScaledBitmap(original, scaledWidth, desiredHeight, false)

        return Bitmap.createBitmap(
                scaledWallpaper,
                widthToCut.toInt(),
                0,
                //                (deviceWidth * 0.97).toInt(),
                deviceWidth,
                desiredHeight
        )
    }

    fun centerCrop(): Bitmap {

        val desiredHeight = myWallpaperManager.desiredMinimumHeight
        val scale = desiredHeight.toFloat() / original.height // ratio between desired and original
        val scaledWidth = (scale * original.width).toInt() // the width after scaling
        val metrics = DisplayMetrics()
        mainActivity.windowManager.defaultDisplay.getMetrics(metrics)
        val deviceWidth = metrics.widthPixels // device width
        val imageCenterWidth = scaledWidth / 2 // centerCrop of original after scaling
        val widthToCut = imageCenterWidth - deviceWidth / 2 //the new scaled centerCrop
        var scaledWallpaper = Bitmap.createScaledBitmap(original, scaledWidth, desiredHeight, false)

        return Bitmap.createBitmap(
                scaledWallpaper,
                widthToCut,
                0,
                //                (deviceWidth * 0.97).toInt(),
                deviceWidth,
                desiredHeight
        )
    }

    private fun addToBundle(bitmap: Bitmap) {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray = stream.toByteArray()
        getBundle()?.putByteArray(MainActivity.IMAGE_KEY, byteArray)
    }

    override fun onHide() {
        if (radioGroup.checkedRadioButtonId == R.id.face_radioButton) {
            addToBundle(face)
        } else {
            addToBundle(center)
        }
    }



    private fun showProgress(show: Boolean) {
        if (show) {
            imageProcessingLinearLayout.visibility = View.VISIBLE
            previewLinearLayout.visibility = View.GONE
        } else {
            imageProcessingLinearLayout.visibility = View.GONE
            previewLinearLayout.visibility = View.VISIBLE
        }
    }

    inner class ImageTask:AsyncTask<Void, Void, List<Bitmap>>() {
        override fun doInBackground(vararg p0: Void?): List<Bitmap> {
            val list = ArrayList<Bitmap>(2)
            list.add(faceCrop())
            list.add(centerCrop())
            return list
        }

        override fun onPostExecute(result: List<Bitmap>?) {
            if (result != null) {
                face = result[0]
                center = result[1]
            }
            showProgress(false)
        }

    }
}