package com.booking.jsalib.facebookwallpaper.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.booking.jsalib.facebookwallpaper.BuildConfig
import com.booking.jsalib.facebookwallpaper.R
import com.booking.jsalib.facebookwallpaper.model.Data
import com.booking.jsalib.facebookwallpaper.ui.Activity.MainActivity
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphResponse
import com.facebook.HttpMethod
import com.google.firebase.crash.FirebaseCrash
import com.google.gson.Gson

@SuppressLint("ValidFragment")
class StepTwoFragment : StepFragment() {
    private var progressBar:ProgressBar? = null
    private var retryButton: Button? = null
    private var infoText: TextView? = null

    private val gson = Gson()

    private val graphCallBack: GraphRequest.Callback = object : GraphRequest.Callback {
        override fun onCompleted(response: GraphResponse?) {
            if (response == null || response.error != null) {
                infoText?.text = "No significant other was found, either check that you have one confirmed on facebook" +
                        "or your internet connection"
                if (BuildConfig.DEBUG) {
                    infoText?.text = "Error in Connection\nMore:"+response?.error
                }
                progressBar?.visibility = View.GONE
                retryButton?.visibility = View.VISIBLE
                return
            }

            val data  = gson.fromJson(response.jsonObject.toString(), Data::class.java)
            val so = data.significantOther
            val bundle = Bundle()
            bundle.putSerializable(MainActivity.SO_KEY, so)
            FirebaseCrash.log("JSON:" + response.jsonObject.toString())
            sendBundle(bundle)
            next()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view  = inflater.inflate(R.layout.step2, container, false)
        if (view != null) {
            infoText = view.findViewById(R.id.info_textView)
            progressBar = view.findViewById(R.id.progressBar)
            retryButton = view.findViewById(R.id.rety_button)
            retryButton?.setOnClickListener({_ ->
                getSignificantOther()
                progressBar?.visibility = View.VISIBLE
                retryButton?.visibility = View.GONE
            })
        }
        return view
    }

    override fun onShown() {
        getSignificantOther()
    }

    private fun getSignificantOther() {
        val parameters = Bundle()
        parameters.putString("fields", "significant_other{name,picture.width(9999)}")
        GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                parameters,
                HttpMethod.GET,
                graphCallBack
        ).executeAsync()
    }

    override fun onHide() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
