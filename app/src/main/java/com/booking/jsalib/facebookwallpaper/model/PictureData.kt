package com.booking.jsalib.facebookwallpaper.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class PictureData(
        @SerializedName("data") val picture: Picture) : Serializable