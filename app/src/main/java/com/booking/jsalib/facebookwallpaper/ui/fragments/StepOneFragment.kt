package com.booking.jsalib.facebookwallpaper.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.booking.jsalib.facebookwallpaper.R
import com.booking.jsalib.facebookwallpaper.ui.Activity.MainActivity
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.firebase.crash.FirebaseCrash

@SuppressLint("ValidFragment")
class StepOneFragment : StepFragment() {
    private val callBackManager: CallbackManager = CallbackManager.Factory.create()
    lateinit var loginButton: LoginButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.step1, container, false)
        if (view != null) {
            loginButton = view.findViewById(R.id.login_button)
            loginButton.setReadPermissions("user_relationships")
            loginButton.registerCallback(
                    callBackManager,
                    object : FacebookCallback<LoginResult> {
                        override fun onError(error: FacebookException?) {
                            Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
                            FirebaseCrash.report(error)
                        }

                        override fun onSuccess(result: LoginResult?) {
                            Toast.makeText(activity, "Success", Toast.LENGTH_LONG).show()
                            next()
                        }

                        override fun onCancel() {
                            Toast.makeText(activity, "Cancel", Toast.LENGTH_LONG).show()
                        }

                    }
            )
        }

        return view
    }

    private fun isLoggedIn(): Boolean {
        val accessToken = AccessToken.getCurrentAccessToken()
        return accessToken != null
    }

    override fun onShown() {
        showNextButton(false)
        showBackButton(false)
        if (isLoggedIn())
            next()
    }

    override fun onHide() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callBackManager.onActivityResult(requestCode, resultCode, data)
    }

}