package com.booking.jsalib.facebookwallpaper.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Picture(
        @SerializedName("url") val url: String,
        @SerializedName("height") val height: Int,
        @SerializedName("width") val width: Int,
        @SerializedName("is_silhouette") val isSilhouette: Boolean) : Serializable