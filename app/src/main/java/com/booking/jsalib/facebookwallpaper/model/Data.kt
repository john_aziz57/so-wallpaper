package com.booking.jsalib.facebookwallpaper.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Data(@SerializedName("id") val name: String,
                @SerializedName("significant_other") val significantOther: SignificantOther) : Serializable