package com.booking.jsalib.facebookwallpaper.ui.fragments

import android.annotation.SuppressLint
import android.app.WallpaperManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Spinner
import com.booking.jsalib.facebookwallpaper.R
import com.booking.jsalib.facebookwallpaper.ui.Activity.MainActivity
import android.graphics.BitmapFactory
import android.graphics.Bitmap



@SuppressLint("ValidFragment")
class StepFiveFragment : StepFragment(){
    private lateinit var doneButton:Button
    private lateinit var spinner:Spinner
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.step5, container, false)
        if (view != null) {
            spinner = view.findViewById(R.id.spinner)
            doneButton = view.findViewById(R.id.done_button)

            doneButton.setOnClickListener({_ -> setUpJob()})
        }

        return view
    }

    private fun setUpJob() {
        val bundle = getBundle()
        if (bundle != null) {
            val byteArray = bundle.getByteArray(MainActivity.IMAGE_KEY)
            val bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
            val myWallpaperManager = WallpaperManager.getInstance(mainActivity)
            myWallpaperManager.setBitmap(bmp)
            next()
        } else {
            //TODO throw an error try to track why the bundle was null
        }
    }

    override fun onShown() {
        showNextButton(false)
        showBackButton(true)
    }

    override fun onHide() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}