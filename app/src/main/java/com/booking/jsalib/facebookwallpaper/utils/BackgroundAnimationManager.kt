package com.booking.jsalib.facebookwallpaper.utils

import android.animation.Animator
import android.animation.ObjectAnimator
import android.view.ViewGroup
import android.content.Context
import android.graphics.Path
import android.view.View
import android.widget.ImageView
import com.booking.jsalib.facebookwallpaper.R
import java.util.*
import kotlin.collections.ArrayList


class BackgroundAnimationManager(context: Context, viewGroup: ViewGroup) {
    private val NUMBER_OF_OBJECTS = 20
    private val imageViewsPool: ArrayList<ImageView>
    private val animatorViewsPool: ArrayList<ObjectAnimator>
    private val animatorListener: Animator.AnimatorListener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(p0: Animator?) {
        }

        override fun onAnimationEnd(p0: Animator?) {
        }

        override fun onAnimationCancel(p0: Animator?) {
        }

        override fun onAnimationStart(p0: Animator?) {
            if (p0 != null) {
                ((p0 as ObjectAnimator).target as View).visibility = View.VISIBLE
                p0.removeAllListeners()
            }
        }
    }

    val imageViewParams = ViewGroup.MarginLayoutParams(
            ViewGroup.MarginLayoutParams.WRAP_CONTENT,
            ViewGroup.MarginLayoutParams.WRAP_CONTENT)

    init {
        val rand = Random()
        imageViewsPool = ArrayList<ImageView>(NUMBER_OF_OBJECTS)
        for (i in 0 until NUMBER_OF_OBJECTS) {
            val imageView = ImageView(context)
            imageView.setImageResource(getImageResource(rand.nextInt(6)))
            imageView.layoutParams = imageViewParams
            imageView.visibility = View.GONE
            imageViewsPool.add(imageView)
            viewGroup.addView(imageView)
        }
        animatorViewsPool = ArrayList<ObjectAnimator>(NUMBER_OF_OBJECTS)
        for (i in 0 until NUMBER_OF_OBJECTS) {
            val imageView = imageViewsPool[i]
            val path = getRandomPath(rand, viewGroup.width, viewGroup.height)
            val objectAnimator = ObjectAnimator.ofFloat(imageView, ImageView.X, ImageView.Y, path)
            objectAnimator.addListener(animatorListener)
            objectAnimator.duration = 3000
            objectAnimator.repeatCount = ObjectAnimator.INFINITE
            objectAnimator.repeatMode = ObjectAnimator.RESTART
            objectAnimator.startDelay = rand.nextInt(4000).toLong()
            animatorViewsPool.add(objectAnimator)
        }
    }

    private fun getImageResource(i: Int): Int {
        return when (i) {
            1 -> R.drawable.heart
            2 -> R.drawable.kiss_eye_closed
            3 -> R.drawable.kiss_eye_open
            4 -> R.drawable.kiss_heart_wink
            else -> R.drawable.love_eyes
        }
    }

    private fun getRandomPath(rand: Random, screenRightBound: Int, screenLowerBound: Int): Path {
        var path = Path()

        path.moveTo(rand.nextInt(screenRightBound).toFloat(), screenLowerBound.toFloat())
        path.lineTo(rand.nextInt(screenRightBound).toFloat(), -200f)
        return path

    }

    fun start() {
        for (animator in animatorViewsPool) {
            animator.start()
        }
    }
}