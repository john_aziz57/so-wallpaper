package com.booking.jsalib.facebookwallpaper.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import com.booking.jsalib.facebookwallpaper.ui.Activity.MainActivity

abstract class StepFragment : Fragment {

    lateinit var mainActivity:MainActivity
    constructor(): super()

    constructor(mainActivity: MainActivity):super() {
        this.mainActivity = mainActivity
    }


    /** sending the main activity the bundle back*/
    fun sendBundle(bundle: Bundle) {
        mainActivity.receiveBundle(bundle)
    }

    fun getBundle():Bundle? {
        return mainActivity.getBundle()
    }

    abstract fun onShown()

    fun showBackButton(show :Boolean) {
        mainActivity.showBackButton(show)
    }

    fun showNextButton(show :Boolean) {
        mainActivity.showNextButton(show)
    }

    fun next() {
        mainActivity.next()
    }

    fun back() {
        mainActivity.back()
    }

    abstract fun onHide()
}