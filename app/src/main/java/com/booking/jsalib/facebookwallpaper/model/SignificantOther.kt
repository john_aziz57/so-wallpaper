package com.booking.jsalib.facebookwallpaper.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SignificantOther(@SerializedName("id") val id: String,
                            @SerializedName("name") val name: String,
                            @SerializedName("picture") val pictureData: PictureData) : Serializable {
    override fun toString(): String {
        return name
    }
}