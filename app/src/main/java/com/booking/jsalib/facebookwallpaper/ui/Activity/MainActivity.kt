package com.booking.jsalib.facebookwallpaper.ui.Activity

import android.content.Context
import android.os.Bundle
import com.booking.jsalib.facebookwallpaper.R
import android.content.Intent
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.Button
import com.badoualy.stepperindicator.StepperIndicator
import com.booking.jsalib.facebookwallpaper.ui.fragments.*
import com.booking.jsalib.facebookwallpaper.ui.pagetransformer.ZoomOutPageTransformer
import com.facebook.stetho.Stetho
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.booking.jsalib.facebookwallpaper.utils.BackgroundAnimationManager
import android.content.pm.PackageManager
import android.util.Base64
import android.util.Log
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class MainActivity : FragmentActivity() {

    companion object {
        val IMAGE_KEY = "IMAGE"
        val SO_KEY = "SO"
    }

    private val NUM_PAGES = 4
    private val FIRST_PAGE = 0

    private val step1Fragment = StepOneFragment()
    private val step2Fragment = StepTwoFragment()
    private val step4Fragment = StepFourFragment()
    private val step5Fragment = StepFiveFragment()

    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: ScreenSlidePagerAdapter
    private lateinit var stepsIndicator: StepperIndicator
    private lateinit var backButton: Button
    private lateinit var nextButton: Button

    private lateinit var frameLayout: FrameLayout

    private var bundle: Bundle? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            val info = packageManager.getPackageInfo(
                    "com.booking.jsalib.facebookwallpaper",
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }

        Stetho.initializeWithDefaults(this)
        setContentView(R.layout.activity_main)

        stepsIndicator = findViewById(R.id.stepper_indicator)
        backButton = findViewById(R.id.back_button)
        backButton.setOnClickListener({ _ -> back() })

        nextButton = findViewById(R.id.next_button)
        nextButton.setOnClickListener({ _ -> next() })

        viewPager = this@MainActivity.findViewById<ViewPager>(R.id.view_pager)
        viewPager.setPageTransformer(true, ZoomOutPageTransformer())
        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                pagerAdapter.getItem(position).onShown()
            }
        })

        pagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)

        viewPager.adapter = pagerAdapter
        stepsIndicator.setViewPager(viewPager, NUM_PAGES - 1)
        frameLayout = findViewById(R.id.animation_frameLayout)

        step1Fragment.mainActivity = this
        step2Fragment.mainActivity = this
        step4Fragment.mainActivity = this
        step5Fragment.mainActivity = this
    }

    override fun onResume() {
        super.onResume()
        // in case it was the first time to open the app and notify the step one fragment that it is being shown
        if (viewPager.currentItem == FIRST_PAGE) pagerAdapter.getItem(viewPager.currentItem).onShown()

        frameLayout.postDelayed({ ->
            BackgroundAnimationManager(this@MainActivity, frameLayout).start()
        }, 100)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val stepFragment: StepFragment = pagerAdapter.getItem(viewPager.currentItem)
        stepFragment.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    inner class ScreenSlidePagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): StepFragment {
//            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            return when (position) {
                1 -> step2Fragment
                2 -> step4Fragment
                3 -> step5Fragment
                else -> step1Fragment
            }
        }

        override fun getCount(): Int {
            return NUM_PAGES
        }
    }

    fun showNextButton(show: Boolean) {
        if (show)
            nextButton.visibility = View.VISIBLE
        else
            nextButton.visibility = View.INVISIBLE
    }

    fun showBackButton(show: Boolean) {
        if (show)
            backButton.visibility = View.VISIBLE
        else
            backButton.visibility = View.INVISIBLE
    }

    fun next() {
        if (viewPager.currentItem < NUM_PAGES -1) {
            pagerAdapter.getItem(viewPager.currentItem).onHide()
            viewPager.currentItem++
            hideKeyboard()
        } else {
            // set the service
            finish()
        }
    }

    fun back() {
//        nextButton.text = "Next"
        if (viewPager.currentItem > 1) {
            viewPager.currentItem--
            hideKeyboard()
        }
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun receiveBundle(bundle: Bundle) {
        if (this.bundle == null) {
            this.bundle = Bundle()
        }
        (this.bundle as Bundle).putAll(bundle)
    }

    fun getBundle(): Bundle {
        if (this.bundle == null)
            bundle = Bundle()
        return bundle as Bundle
    }

}
